﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace com.FunJimChee.FileStructTool
{
    public static class ExtensionMethod
    {
        private static List<Type> _SystemTypes;
        public static bool IsList(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
        }

        public static bool IsDictionary(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>);
        }

        public static bool IsSystemType(this Type type)
        {
            _SystemTypes ??= Assembly.GetExecutingAssembly().GetType().Module.Assembly.GetExportedTypes().ToList();

            return _SystemTypes.Where(sysType => sysType == type).ToArray().Length > 0;
        }
    }
}