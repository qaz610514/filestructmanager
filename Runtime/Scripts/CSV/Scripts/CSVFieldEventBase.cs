﻿using System;
using System.Reflection;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class CSVFieldEventBase
    {
        public (string Title, string Content) GetCsvItemInfo(FieldInfo fieldInfo, object fieldValue)
        {
            if (fieldInfo == null) return FieldInfoNullEvent(fieldValue);

            if (fieldValue == null) return FieldValueNullEvent(fieldInfo);

            return GetHandle(fieldInfo, fieldValue);
        }

        private (string Title, string Content) FieldInfoNullEvent(object fieldValue)
        {
            //can do something preprocess

            return fieldValue == null
                ? ("Error Convert", "The fieldInfo and fieldValue are null!")
                : OnFieldInfoNullEvent(fieldValue);
        }

        private (string Title, string Content) GetHandle(FieldInfo fieldInfo, object fieldValue)
        {
            //can do something preprocess

            if (fieldValue == null) return FieldInfoNullEvent(fieldInfo);

            return OnHandleValueEvent(fieldInfo.FieldType, fieldInfo.Name, fieldValue.ToString());
        }

        private (string Title, string Content) FieldValueNullEvent(FieldInfo fieldInfo)
        {
            //can do something preprocess

            return OnHandleValueNullEvent(fieldInfo.FieldType, fieldInfo.Name);
        }

        protected virtual (string Title, string Content) OnFieldInfoNullEvent(object fieldValue)
        {
            return fieldValue.GetType().IsSystemType() ? ("",fieldValue.ToString()) : ("Error Convert", "The fieldInfo is null!");
        }

        protected virtual (string Title, string Content) OnHandleValueNullEvent(Type fieldType, string fieldName)
        {
            return (fieldName, "type:struct Null");
        }

        protected virtual (string Title, string Content) OnHandleValueEvent(Type fieldType, string fieldName, string fieldValue)
        {
            return (fieldName, fieldValue);
        }
    }
}