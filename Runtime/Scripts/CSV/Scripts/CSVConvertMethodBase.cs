﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace com.FunJimChee.FileStructTool.CSV
{
    public abstract class CSVConvertMethodBase
    {
        public abstract CSVConvertMethodType MethodType { get; }

        private CSVFieldEventBase _fieldEvent;

        protected CSVFieldEventBase FieldEvent => _fieldEvent ?? CSVConvertManagerByDefault.FieldEvent;

        private CSVHandleReportBase _handleReport;

        private CSVHandleReportBase HandleReport => _handleReport ?? CSVConvertManagerByDefault.HandleReport;

        private CSVConvertMethodFactoryBase _factory;

        protected CSVConvertMethodFactoryBase Factory => _factory ?? new CSVConvertMethodFactoryBase();

        public bool OverrideFieldEvent(CSVFieldEventBase fieldEvent)
        {
            if (fieldEvent == null) return false;

            _fieldEvent = fieldEvent;

            return true;
        }

        public bool OverrideHandleReport(CSVHandleReportBase handleReport)
        {
            if (handleReport == null) return false;

            _handleReport = handleReport;

            return true;
        }

        protected abstract List<List<string>> Convert(object data);

        protected abstract List<List<string>> Convert(FieldInfo fieldInfo, object data);

        public List<List<string>> GetConvertHandle(object data, List<List<string>> currentHandle)
        {
            currentHandle.AddRange(Convert(data));

            currentHandle = HandleReport.GetConvertHandle(data.GetType(), currentHandle);

            return currentHandle;
        }

        public List<List<string>> GetConvertHandle(FieldInfo fieldInfo, object data, List<List<string>> currentHandle)
        {
            currentHandle.AddRange(Convert(fieldInfo, data));

            currentHandle = HandleReport.GetConvertHandle(fieldInfo.Name, fieldInfo.FieldType, currentHandle);

            return currentHandle;
        }
    }
}