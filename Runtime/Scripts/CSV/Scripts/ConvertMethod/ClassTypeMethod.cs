﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class ClassTypeMethod : CSVConvertMethodBase
    {
        public override CSVConvertMethodType MethodType => CSVConvertMethodType.Class;
        protected override List<List<string>> Convert(object data)
        {
            var fieldInfos = data.GetType().GetFields();

            var handle = new List<List<string>>();

            foreach (var fieldInfo in fieldInfos)
            {
                var fieldData = fieldInfo.GetValue(data);
                
                var method = Factory.GetConvertMethod(CSVConvertManagerByDefault.GetMethodType(fieldData));

                handle = method.GetConvertHandle(fieldInfo, fieldData, handle);
            }
            
            return handle;
        }

        protected override List<List<string>> Convert(FieldInfo fieldInfo, object data)
        {
            var fieldInfos = data.GetType().GetFields();

            var handle = new List<List<string>>();

            foreach (var info in fieldInfos)
            {
                var fieldData = fieldInfo.GetValue(data);
                
                var method = Factory.GetConvertMethod(CSVConvertManagerByDefault.GetMethodType(fieldData));

                handle = method.GetConvertHandle(info, fieldData, handle);
            }
            
            return handle;
        }
    }
}