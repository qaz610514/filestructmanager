﻿using System.Collections.Generic;
using System.Reflection;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class SystemTypeMethod : CSVConvertMethodBase
    {
        public override CSVConvertMethodType MethodType => CSVConvertMethodType.SystemType;

        protected override List<List<string>> Convert(object data)
        {
            var (t, c) = FieldEvent.GetCsvItemInfo(null, data);

            return new List<List<string>> { new List<string> { t, c } };
        }

        protected override List<List<string>> Convert(FieldInfo fieldInfo, object data)
        {
            var (t, c) = FieldEvent.GetCsvItemInfo(fieldInfo, data);

            return new List<List<string>> { new List<string> { t, c } };
        }
    }
}