﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class DictionaryTypeMethod : CSVConvertMethodBase
    {
        public override CSVConvertMethodType MethodType => CSVConvertMethodType.Dictionary;

        protected override List<List<string>> Convert(object data)
        {
            var dictionary = (IDictionary)data;

            var newDictionary = CastDict(dictionary).ToDictionary(entry => entry.Key, entry => entry.Value);

            var handle = new List<List<string>>();

            if (newDictionary.Count > 0)
            {
                var dValues = newDictionary.Select(item => item.Value).ToList();

                if (dValues.Count > 0)
                {
                    var method = Factory.GetConvertMethod(CSVConvertManagerByDefault.GetMethodType(dValues[0]));

                    foreach (var item in dValues)
                    {
                        handle = method.GetConvertHandle(item, handle);
                    }
                }
            }

            return handle;
        }

        protected override List<List<string>> Convert(FieldInfo fieldInfo, object data)
        {
            var dictionary = (IDictionary)data;

            var newDictionary = CastDict(dictionary).ToDictionary(entry => entry.Key, entry => entry.Value);

            var handle = new List<List<string>>();

            if (newDictionary.Count > 0)
            {
                var dValues = newDictionary.Select(item => item.Value).ToList();

                if (dValues.Count > 0)
                {
                    var method = Factory.GetConvertMethod(CSVConvertManagerByDefault.GetMethodType(dValues[0]));

                    foreach (var item in dValues)
                    {
                        handle = method.GetConvertHandle(item, handle);
                    }
                }
            }

            return handle;
        }

        private IEnumerable<DictionaryEntry> CastDict(IDictionary dictionary)
        {
            foreach (DictionaryEntry entry in dictionary)
            {
                yield return entry;
            }
        }
    }
}