﻿using System.Collections.Generic;
using System.Reflection;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class EmptyTypeMethod : CSVConvertMethodBase
    {
        public override CSVConvertMethodType MethodType => CSVConvertMethodType.Empty;
        protected override List<List<string>> Convert(object data)
        {
            return new List<List<string>>{new List<string>{"Error","null"}};
        }

        protected override List<List<string>> Convert(FieldInfo fieldInfo, object data)
        {
            return new List<List<string>>{new List<string>{"Error","null"}};
        }
    }
}