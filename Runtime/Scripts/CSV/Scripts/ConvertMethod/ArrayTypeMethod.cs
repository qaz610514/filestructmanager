﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class ArrayTypeMethod : CSVConvertMethodBase
    {
        public override CSVConvertMethodType MethodType => CSVConvertMethodType.ArrayOrList;

        protected override List<List<string>> Convert(object data)
        {
            var list = (IList)data;

            var handle = new List<List<string>>();

            if (list.Count != 0)
            {
                var method = Factory.GetConvertMethod(CSVConvertManagerByDefault.GetMethodType(list[0]));

                foreach (var item in list)
                {
                    handle = method.GetConvertHandle(item, handle);
                }
            }

            return handle;
        }

        protected override List<List<string>> Convert(FieldInfo fieldInfo, object data)
        {
            var list = (IList)data;

            var handle = new List<List<string>>();

            if (list.Count != 0)
            {
                var method = Factory.GetConvertMethod(CSVConvertManagerByDefault.GetMethodType(list[0]));

                foreach (var item in list)
                {
                    handle = method.GetConvertHandle(item, handle);
                }
            }

            return handle;
        }
    }
}