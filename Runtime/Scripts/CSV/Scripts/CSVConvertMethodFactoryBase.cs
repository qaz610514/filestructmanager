﻿using System.Collections.Generic;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class CSVConvertMethodFactoryBase
    {
        private Dictionary<CSVConvertMethodType, CSVConvertMethodBase> _allMethods =
            new Dictionary<CSVConvertMethodType, CSVConvertMethodBase>(5);

        public bool AddOrOverrideMethod(CSVConvertMethodType methodType, CSVConvertMethodBase convertMethod)
        {
            if (convertMethod == null) return false;
            
            if(!_allMethods.ContainsKey(methodType)) _allMethods.Add(methodType, null);

            _allMethods[methodType] = convertMethod;

            return true;
        }

        public CSVConvertMethodBase GetConvertMethod(CSVConvertMethodType methodType)
        {
            if (_allMethods.ContainsKey(methodType)) return _allMethods[methodType];

            return CSVConvertManagerByDefault.GetConvertMethod(methodType);
        }
    }
}