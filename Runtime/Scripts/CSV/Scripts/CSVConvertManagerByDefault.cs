﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace com.FunJimChee.FileStructTool.CSV
{
    public static class CSVConvertManagerByDefault
    {
        private static CSVFieldEventBase _fieldEvent;

        public static CSVFieldEventBase FieldEvent => _fieldEvent ?? new CSVFieldEventBase();

        private static CSVHandleReportBase _handleReport;

        public static CSVHandleReportBase HandleReport => _handleReport ?? new CSVHandleReportBase();

        private static CSVConvertMethodFactoryBase _factory;

        private static CSVConvertMethodFactoryBase Factory => _factory ?? GetDefaultFactory();

        private static CSVConvertMethodFactoryBase GetDefaultFactory()
        {
            //TODO wait to setting

            //use reflection

            var defaultFactory = new CSVConvertMethodFactoryBase();

            defaultFactory.AddOrOverrideMethod(CSVConvertMethodType.Empty, new EmptyTypeMethod());
            defaultFactory.AddOrOverrideMethod(CSVConvertMethodType.SystemType, new SystemTypeMethod());
            defaultFactory.AddOrOverrideMethod(CSVConvertMethodType.ArrayOrList, new ArrayTypeMethod());
            defaultFactory.AddOrOverrideMethod(CSVConvertMethodType.Dictionary, new DictionaryTypeMethod());
            defaultFactory.AddOrOverrideMethod(CSVConvertMethodType.Class, new ClassTypeMethod());

            return defaultFactory;
        }

        public static bool OverrideFieldEvent(CSVFieldEventBase fieldEvent)
        {
            if (fieldEvent == null) return false;

            _fieldEvent = fieldEvent;

            return true;
        }

        public static bool OverrideHandleReport(CSVHandleReportBase handleReport)
        {
            if (handleReport == null) return false;

            _handleReport = handleReport;

            return true;
        }

        public static bool OverrideConvertMethod(CSVConvertMethodType methodType, CSVConvertMethodBase convertMethod)
        {
            return Factory.AddOrOverrideMethod(methodType, convertMethod);
        }

        public static CSVConvertMethodBase GetConvertMethod(CSVConvertMethodType methodType)
        {
            return Factory.GetConvertMethod(methodType);
        }

        public static List<List<string>> GetHandle(object data)
        {
            if (data == null) return null;
        
            var convertType = GetMethodType(data);
        
            if (convertType == CSVConvertMethodType.Empty) return null;
        
            var method = Factory.GetConvertMethod(convertType);

            return method.GetConvertHandle(data, new List<List<string>>());
        }
        
        //
        // public static List<List<string>> GetHandle(FieldInfo fieldInfo, object data)
        // {
        //     if (data == null) return null;
        //
        //     var convertType = GetMethodType(data);
        //
        //     if (convertType == CSVConvertMethodType.Empty) return null;
        //
        //     var method = Factory.GetConvertMethod(convertType);
        //
        //     return method.GetConvertHandle(fieldInfo, data, new List<List<string>>());
        // }

        public static CSVConvertMethodType GetMethodType(object data)
        {
            if (data == null) return CSVConvertMethodType.Empty;
            
            var dataType = data.GetType();

            if (dataType.IsSystemType()) return CSVConvertMethodType.SystemType;

            if (dataType.IsArray || dataType.IsList()) return CSVConvertMethodType.ArrayOrList;

            if (dataType.IsDictionary()) return CSVConvertMethodType.Dictionary;

            if (dataType.IsClass) return CSVConvertMethodType.Class;

            return CSVConvertMethodType.Empty;
        }
    }
}