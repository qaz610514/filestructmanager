﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace com.FunJimChee.FileStructTool.CSV
{
    public static class CSVConvertMangaer
    {
        public static (List<string>Titles, List<string>Contents) ConvertClass(object data)
        {
            if (data == null)
            {
                return (new List<string> { default }, new List<string> { "type:struct Null" });
            }

            var fieldType = data.GetType();

            var titles = new List<string> { fieldType.Name };
            var contents = new List<string> { "type:struct Start" };

            var (t, c) = GetClassFieldsInfo(data);
            titles.AddRange(t);
            contents.AddRange(c);

            titles.Add(fieldType.Name);
            contents.Add("type:struct End");

            return (titles, contents);
        }

        public static (List<string>Titles, List<string>Contents) ConvertClass(FieldInfo fieldInfo, object data)
        {
            if (data == null)
            {
                return (new List<string> { fieldInfo.Name }, new List<string> { "type:struct Null" });
            }
            
            var fieldType = data.GetType();

            var titles = new List<string> { fieldInfo.Name, fieldType.Name };
            var contents = new List<string> { default, "type:struct Start" };

            var (t, c) = GetClassFieldsInfo(data);
            titles.AddRange(t);
            contents.AddRange(c);

            titles.Add(fieldType.Name);
            contents.Add("type:struct End");

            return (titles, contents);
        }

        public static (List<string>Titles, List<string> Contents) ConvertListOrArray(IList fieldList)
        {
            if (fieldList == null)
            {
                return (new List<string> { default }, new List<string> { "type:Array Null" });
            }

            var itemType = fieldList[0].GetType();

            var titles = new List<string> { itemType.Name };

            var contents = new List<string> { "type:Array Start" };

            var (t, c) = GetListFieldsInfo(itemType, fieldList);
            titles.AddRange(t);
            contents.AddRange(c);

            titles.Add(itemType.Name);
            contents.Add("type:Array End");

            return (titles, contents);
        }

        public static (List<string>Titles, List<string> Contents) ConvertListOrArray(FieldInfo fieldInfo,
            IList fieldList)
        {
            if (fieldList == null)
            {
                return (new List<string> { fieldInfo.Name }, new List<string> { "type:Array Null" });
            }

            var titles = new List<string> { fieldInfo.Name };

            var contents = new List<string> { "type:Array Start" };

            var itemType = fieldList[0].GetType();

            var (t, c) = GetListFieldsInfo(itemType, fieldList);
            titles.AddRange(t);
            contents.AddRange(c);

            titles.Add(fieldInfo.Name);
            contents.Add("type:Array End");

            return (titles, contents);
        }

        private static (List<string>Titles, List<string>Contents) GetClassFieldsInfo(object data)
        {
            var titles = new List<string>();

            var contents = new List<string>();

            var fieldType = data.GetType();

            var fields = fieldType.GetFields(BindingFlags.Public | BindingFlags.Instance);

            for (var index = 0; index < fields.Length; index++)
            {
                var field = fields[index];

                var itemType = field.FieldType;

                var fieldContent = field.GetValue(data);

                if (itemType.IsSystemType())
                {
                    if (fieldContent == null)
                    {
                        titles.Add(field.Name);
                        contents.Add("type:struct Null");
                    }
                    else
                    {
                        titles.Add(field.Name);
                        contents.Add(fieldContent.ToString());
                    }

                    continue;
                }

                if (itemType.IsArray || itemType.IsList())
                {
                    var (t, c) = ConvertListOrArray(field, (IList)fieldContent);

                    titles.AddRange(t);
                    contents.AddRange(c);
                    continue;
                }

                var (t2, c2) = ConvertClass(field, fieldContent);
                titles.AddRange(t2);
                contents.AddRange(c2);
            }

            return (titles, contents);
        }

        private static (List<string>Titles, List<string>Contents) GetListFieldsInfo(Type itemType, IList fieldList)
        {
            var titles = new List<string>();

            var contents = new List<string>();

            if (itemType.IsSystemType())
            {
                foreach (var data in fieldList)
                {
                    titles.Add(default);
                    contents.Add(data.ToString());
                }
            }
            else
            {
                var rows = itemType.GetFields(BindingFlags.Public | BindingFlags.Instance);

                if (itemType.IsSystemType())
                {
                    foreach (var data in fieldList)
                    {
                        foreach (var row in rows)
                        {
                            titles.Add(row.Name);
                            contents.Add(row.GetValue(data).ToString());
                        }
                    }
                }
                else if (itemType.IsArray || itemType.IsList())
                {
                    foreach (var data in fieldList)
                    {
                        foreach (var row in rows)
                        {
                            var (t, c) = ConvertListOrArray(row, (IList)row.GetValue(data));
                            titles.AddRange(t);
                            contents.AddRange(c);
                        }
                    }
                }
                else
                {
                    foreach (var data in fieldList)
                    {
                        var (t, c) = ConvertClass(data);
                        titles.AddRange(t);
                        contents.AddRange(c);
                    }
                }
            }

            return (titles, contents);
        }
    }
}