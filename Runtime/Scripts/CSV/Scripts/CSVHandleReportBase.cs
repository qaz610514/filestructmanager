﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace com.FunJimChee.FileStructTool.CSV
{
    public class CSVHandleReportBase
    {
        public virtual List<List<string>> GetConvertHandle(Type structType, List<List<string>> currentHandle)
        {
            //TODO add default handle title and content

            return currentHandle;
        }

        public virtual List<List<string>> GetConvertHandle(string fieldName, Type structType,
            List<List<string>> currentHandle)
        {
            //TODO add default handle title and content

            if (structType.IsSystemType()) return currentHandle;

            String title;

            if (structType.IsArray || structType.IsList() || structType.IsDictionary())
            {
                title = "type:Array";
            }
            else
            {
                title = "type:class";
            }

            var newHandle = currentHandle.Select(list =>
            {
                list.Insert(0, string.Empty);
                list.Insert(0, string.Empty);
                return list;
            }).ToList();

            newHandle.Insert(0, new List<string> { fieldName, $"{title} Start", "", "" });
            newHandle.Add(new List<string> { fieldName, $"{title} End", "", "" });

            return newHandle;
        }
    }
}