﻿namespace com.FunJimChee.FileStructTool.CSV
{
    public enum CSVConvertMethodType
    {
        Empty,
        SystemType,
        ArrayOrList,
        Class,
        Dictionary
    }
}