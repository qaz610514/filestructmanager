﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using com.FunJimChee.FileStructTool.CSV;
using com.FunJimChee.FileStructTool.Interface;
using UnityEngine;

namespace com.FunJimChee.FileStructTool
{
    public class CSVMethod : IFileMethod
    {
        public bool ConvertStruct<T>(T data, out string handleResult)
        {
            try
            {
                var handle = CSVConvertManagerByDefault.GetHandle(data);

                var result = string.Empty;

                foreach (var list in handle)
                {
                    var info = list.Aggregate((pre, current) => $"{pre},{current}");
                    
                    if (string.IsNullOrEmpty(result))
                    {
                        result = info;
                        continue;
                    }

                    result = $"{result}\r\n{info}";
                }

                handleResult = result;

                return true;
            }
            catch (Exception e)
            {
                handleResult = "";
                
                return false;
            }
            
            // try
            // {
            //     List<string> titles, contents;
            //
            //     var contentFieldInfo = data.GetType().GetField("HandleData");
            //
            //     var contentField = contentFieldInfo.GetValue(data);
            //
            //     var contentFieldType = contentFieldInfo.FieldType;
            //
            //     if (contentFieldType.IsArray || contentFieldType.IsCanConvertList())
            //     {
            //         (titles, contents) = CSVConvertMangaer.ConvertListOrArray((IList)contentField);
            //     }
            //     else
            //     {
            //         (titles, contents) = CSVConvertMangaer.ConvertClass(contentField);
            //     }
            //
            //
            //     handleResult = "DataStruct,DataEvent,Title,Content";
            //
            //     for (var i = 0; i < titles.Count; i++)
            //     {
            //         var content = $"{titles[i]},{contents[i]}";
            //
            //         if (content.Contains("type:"))
            //         {
            //             content = $"{content},,";
            //         }
            //         else
            //         {
            //             content = $",,{content}";
            //         }
            //
            //         handleResult = $"{handleResult}\r\n{content}";
            //     }
            //
            //     return true;
            // }
            // catch (Exception e)
            // {
            //     Debug.LogWarning(e.Message);
            //     handleResult = default;
            //     return false;
            // }
        }
    }
}