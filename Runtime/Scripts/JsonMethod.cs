﻿using System;
using com.FunJimChee.FileStructTool.Interface;
using Newtonsoft.Json;

namespace com.FunJimChee.FileStructTool
{
    public class JsonMethod : IFileMethod
    {
        public bool ConvertStruct<T>(T data, out string handleResult)
        {
            try
            {
                handleResult = JsonConvert.SerializeObject(data);
                return true;
            }
            catch (Exception e)
            {
                handleResult = default;
                return false;
            }
        }
    }
}