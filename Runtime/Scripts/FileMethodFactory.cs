﻿using com.FunJimChee.FileStructTool.Interface;

namespace com.FunJimChee.FileStructTool
{
    public static class FileMethodFactory
    {
        public static IFileMethod GetMethod(FileType fileType)
        {
            return fileType switch
            {
                FileType.Csv => new CSVMethod(),
                FileType.Json => new JsonMethod(),
                _ => new JsonMethod()
            };
        }
    }
}