﻿namespace com.FunJimChee.FileStructTool
{
    public static partial class FileStructManager
    {
        public static string ConvertData<T>(T data, FileType fileType)
        {
            var method = FileMethodFactory.GetMethod(fileType);

            var result = method.ConvertStruct(data, out var convertResult);

            return convertResult;
        }
    }
}