﻿namespace com.FunJimChee.FileStructTool.Interface
{
    public interface IFileMethod
    {
        bool ConvertStruct<T>(T data, out string handleResult);
    }
}